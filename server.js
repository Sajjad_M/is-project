const express = require('express');
const app = express();
const mongoose = require('mongoose');
const authRoutes = require('./routes/auth');
const contentRoutes = require('./routes/content');
const settingRoutes = require('./routes/setting');

const {checkUser, checkAdmin}= require('./middleware/authMiddle.js')

const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
require('dotenv').config();


app.use(express.json());
app.use(express.static('public'));
app.use(cookieParser())

app.set('view engine', 'ejs');

mongoose.connect('mongodb://localhost/CMS',{ useNewUrlParser: true,  useUnifiedTopology: true})
.then(()=>{
  app.listen(3000);
})

app.get('*', checkUser);
app.use(authRoutes);
app.use(contentRoutes);
app.use(checkAdmin, settingRoutes);

