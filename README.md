## Setup and Requirements

For running this code you need [node.js](https://nodejs.org/en/download) and [mongodb](https://www.mongodb.com/try/download/community) <br />
Then run the following commands

```
git clone https://gitlab.com/Sajjad_M/is-project.git
```
go to the created directory and run:
```
npm install 
```
Then make a .env file in the root directory of project with following variable.
```
JWT_SECRET=your jwt secret
```
After that you can use following commands for running
```
npm run start 