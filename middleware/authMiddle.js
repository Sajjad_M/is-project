const jwt = require('jsonwebtoken');
const User = require("../models/User");
const Site = require("../models/Site");

//check if it is admin
const checkAdmin = (req, res, next) => {
  const token = req.cookies.jwt;
  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, async (err, decode) => {
      const user = await User.findById(decode.id);
      if (user) {
        req.user = user;
        next();
      } else {
        res.redirect("/Home")
      }
    })
  } else {
    res.redirect("/Home");
  }
}
//check the user authorization
const checkUser = async (req, res, next) => {
  const site = await Site.find();
  if(site.length === 1){
    res.locals.sitename = site[0].name;
    const token = req.cookies.jwt;
    if (token) {
      jwt.verify(token, process.env.JWT_SECRET, async (err, decodedToken) => {
        if (err) {
          res.locals.username = null;
          res.locals.isAdmin = false;
          next();
        } else {
          let user = await User.findById(decodedToken.id);
          res.locals.username = user.username;
          res.locals.isAdmin = true;
          next();
        }
      });
    } else {
      res.locals.username = null;
      res.locals.isAdmin = false;
      next();
    }
  }else{
     res.redirect("/adminsignup");
  }
};
module.exports = { checkAdmin, checkUser }
