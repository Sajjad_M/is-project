const form = document.querySelector('#signup');
const chagenPassForm = document.querySelector('#change-pass');
form.addEventListener('submit', async (e) => {
    e.preventDefault();

    const email = form.email.value;
    const username = form.username.value;
    const fname = form.fname.value;
    const lname = form.lname.value;
    try {
        const res = await fetch('/InfoSetting', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, email, fname, lname })
        });
        const data = await res.json();

        if (data.msg === "ok") {
            location.assign('/Home');
        }


    }
    catch (err) {
        console.log(err);
    }
});
chagenPassForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const oldpass = chagenPassForm.oldpass.value;
    const newpass = chagenPassForm.newpass.value;
    const newpassRepeat = chagenPassForm.newpassRepeat.value;
    const passError = document.querySelector('.pass.error');

    if (newpass === newpassRepeat) {
        console.log('not err')

        try {
            const res = await fetch('/ChangePass', {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ oldpass, newpass })
            });
            const data = await res.json();

            if (data.msg === "ok") {
                window.location.href = data.redirect;
            }

        }
        catch (err) {
            console.log(err);
        }
    } else {
        console.log('err')
        passError.textContent = "Passwords don't match";
    }
});