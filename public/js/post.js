const trashcan = document.querySelector('a.delete');
const edit = document.querySelector('a.edit');
const endpoint = `/Post/${trashcan.dataset.doc}`;
const newPostBtn = document.querySelector('.new-btn')
document.getElementById('title').value = postTitle
document.getElementById('description').value = postDescription

trashcan.addEventListener('click', (e) => {

    fetch(endpoint, {
        method: 'DELETE',
    })
        .then(response => response.json())
        .then(data => window.location.href = data.redirect)
        .catch(err => console.log(err));
});
edit.addEventListener('click', (e) => {

    document.getElementById("text-content").style.display = "none";
    document.getElementById("edit-form").style.display = "block";

});

document.getElementById("edit-form").style.display = "none";
DecoupledEditor
    .create(document.querySelector('.document-editor__editable'), { initialData: postContent })
    .then((editor) => {
        const toolbarContainer = document.querySelector('.document-editor__toolbar');
        toolbarContainer.appendChild(editor.ui.view.toolbar.element);

        newPostBtn.addEventListener('click', (e) => {
            const title = document.getElementById('title').value;
            const description = document.getElementById('description').value;
            console.log(title)
            fetch(endpoint, {
                method: 'PUT',
                body: JSON.stringify({ title, content: editor.getData(), description }),
                headers: { 'Content-Type': 'application/json' }
            })
                .then(response => response.json())
                .then(data => window.location.href = data.redirect)
                .catch(err => console.log(err));
        });

    })
    .catch(error => {
        console.error(error);
    });
const date = new Date(postDate).toLocaleString()
document.getElementById("date").innerText = date;