const form = document.querySelector('form');
form.addEventListener('submit', async (e) => {
    e.preventDefault();

    const siteName = form.siteName.value;
    try {
        const res = await fetch('/SiteSetting', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ siteName })
        });
        const data = await res.json();

        if (data.msg === "ok") {
            window.location.href = data.redirect;
        }


    }
    catch (err) {
        console.log(err);
    }
});