
const newPostBtn = document.querySelector('.new-btn')
DecoupledEditor
    .create(document.querySelector('.document-editor__editable'))
    .then((editor) => {
        const toolbarContainer = document.querySelector('.document-editor__toolbar');
        toolbarContainer.appendChild(editor.ui.view.toolbar.element);

        newPostBtn.addEventListener('click', async () => {

            const title = document.getElementById('title').value;
            const description = document.getElementById('description').value;

            const res = await fetch('/NewPost', {
                method: 'POST',
                body: JSON.stringify({ title, content: editor.getData(), description }),
                headers: { 'Content-Type': 'application/json' }
            }).then(response => response.json())
                .then(data => window.location.href = data.redirect)

        })

    })
    .catch(error => {
        console.error(error);
    });