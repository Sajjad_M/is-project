const mongoose = require('mongoose');


const postSchema = new mongoose.Schema({
    title: {
      type: String  
    },
    content: {
      type: String
    },
    description:{
      type: String
    },
    view:{
      type: Number
    },
    postdate:{
      type: Date
    }
  });
  
  const Post = mongoose.model('post', postSchema);

  module.exports = Post;  