const mongoose = require('mongoose');


const siteSchema = new mongoose.Schema({
    name: {
      type: String  
    }
  });
  
  const Site = mongoose.model('site', siteSchema);

  module.exports = Site;  