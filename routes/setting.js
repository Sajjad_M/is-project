const {Router} = require("express");
const settingControllers = require("../controllers/settingControllers");

const router = Router();

router.get("/Setting", settingControllers.setting_get);
router.get("/InfoSetting", settingControllers.infoSetting_get);
router.put("/InfoSetting", settingControllers.infoSetting_put);

router.get("/SiteSetting", settingControllers.siteSetting_get);
router.put("/SiteSetting", settingControllers.siteSetting_put);
router.put("/ChangePass", settingControllers.ChangePass_put);


module.exports = router;