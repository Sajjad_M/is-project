const {Router} = require("express");
const contentControllers = require("../controllers/contentControllers");

const router = Router();

router.get("/Home", contentControllers.home_get);
router.get("/Posts", contentControllers.posts_get);
router.post("/NewPost", contentControllers.newPost_post);
router.get("/NewPost", contentControllers.newPost_get);
router.get("/Post/:id", contentControllers.post_get);
router.delete("/Post/:id", contentControllers.post_delete);
router.put("/Post/:id", contentControllers.post_put);


module.exports = router;