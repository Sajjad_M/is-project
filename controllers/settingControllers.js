const User = require("../models/User");
const Site = require("../models/Site");

module.exports.setting_get =async (req , res) =>{
    res.render('Setting');  
}
module.exports.infoSetting_get=async(req, res)=>{
    res.render('InfoSetting', {user:req.user});
}

module.exports.infoSetting_put=async(req, res)=>{
    req.user.username=req.body.username;
    req.user.email=req.body.email;
    req.user.name=req.body.fname;
    req.user.lastName=req.body.lname;
    req.user.save();
    res.status(201).json({ msg:"ok" });
}

module.exports.siteSetting_get=async(req, res)=>{
    const site = await Site.find();
    const sitename = site[0].name;
    res.render('SiteSetting', {user:req.user, sitename});
}

module.exports.siteSetting_put=async(req, res)=>{
    const site = await Site.find();
    site[0].name=req.body.siteName;
    site[0].save();
    res.status(201).json({ msg:"ok", redirect: '/Home' });
}
module.exports.ChangePass_put=async(req, res)=>{
    const isMatch=await User.comparePass(req.user.password, req.body.oldpass);  
    if(isMatch){
        req.user.password=req.body.newpass;
        req.user.save();
        res.status(201).json({ msg:"ok", redirect: '/Home' });
    }else{
        res.status(400).json({ error: "Old password didn't match" });
    }
    
}

