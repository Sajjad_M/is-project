const jwt = require('jsonwebtoken');
const User = require("../models/User");
const Post = require("../models/Post");

module.exports.home_get = async (req , res) =>{
    const posts = await Post.find();
    res.render('Home', {posts});                                                   
}

module.exports.newPost_get = (req , res) =>{
    const cookieJWT = req.cookies.jwt;
    jwt.verify(cookieJWT, process.env.JWT_SECRET, async (err, decode)=>{
        try{
            const user = await User.findById(decode.id);
            if(user){
                res.render('NewPost');      
            }else{
                res.redirect("/Home")      
            }
        }catch(err){
            res.redirect("/Home")
        }
                                                     
    })
}

module.exports.newPost_post = (req , res) =>{
    const cookieJWT = req.cookies.jwt;
    jwt.verify(cookieJWT, process.env.JWT_SECRET, async (err, decode)=>{
        if(err){
            console.log(err);
        } else{
            const user = User.findById(decode.id);
            if(user){
                const date = new Date();
                const post = await Post.create({title: req.body.title, content:req.body.content , description: req.body.description, view:0, postdate:date});  
                post.save();
                res.json({ redirect: '/Home' });
            }
            res.status(401);
        }
    })
 }

 module.exports.posts_get =async (req , res) =>{
  
    const posts = await Post.find();
    res.render('Posts', {posts});
     
 }
 module.exports.post_get =async (req , res) =>{
    const post = await Post.findById(req.params.id);
    post.view+=1;
    post.save();
    res.render('Post', {post});
     
 }


 module.exports.post_delete= async(req, res)=>{
    const cookieJWT = req.cookies.jwt;
    jwt.verify(cookieJWT, process.env.JWT_SECRET, async (err, decode)=>{
       const user = User.findById(decode.id);
       if(user){
           await Post.deleteOne({_id:req.params.id});
           res.json({ redirect: '/Home' });
       }
    })
 }
 module.exports.post_put= async(req, res)=>{
    const cookieJWT = req.cookies.jwt;
    jwt.verify(cookieJWT, process.env.JWT_SECRET, async (err, decode)=>{
       const user = User.findById(decode.id);
       if(user){
            const post = await Post.findById(req.params.id);
            post.content=req.body.content;
            post.title=req.body.title;
            post.description=req.body.description;
            post.save();
            res.json({ redirect: '/Home' });
       }
    })
 }